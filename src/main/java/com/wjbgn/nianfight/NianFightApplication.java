package com.wjbgn.nianfight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NianFightApplication {

    public static void main(String[] args) {
        SpringApplication.run(NianFightApplication.class, args);
    }

}
